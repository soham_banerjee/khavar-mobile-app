<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link rel="stylesheet" href="carousel/carousel.css">
<link rel="stylesheet" href="carousel/carousel-style.css">

<link rel="stylesheet" href="css/main.css">
</head>

<body>
<?php
$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
if(strpos($ua,'khavar') == false)
{
//error code goes here
echo '<h1 class="shopname">Please visit this site on a mobile device!</h1>';
exit();
}
?>
<?php
$con=mysqli_connect("localhost","cryllsof_khavar","sulfisoxazole","cryllsof_khavar");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$result = mysqli_query($con,"SELECT * FROM shops ORDER BY votes DESC");

$count=0;
while($row = mysqli_fetch_array($result))
{
	$images[$count]=$row['IMAGE'];
	$names[$count]=$row['NAME'];
	$link[$count]=$row['ID'];
	$count++;
}



?>

<div class="m-carousel m-fluid m-carousel-photos">
  <div class="m-carousel-inner">
      <a class="m-item" href="showshop.php?resid=<?php echo $link[0]?>">
      <img src="<?php echo $images[0]?>">
      <p class="m-caption"><?php echo $names[0]?></p>
      </a>
      <a class="m-item" href="showshop.php?resid=<?php echo $link[1]?>">
      <img src="<?php echo $images[1]?>">
      <p class="m-caption"><?php echo $names[1]?></p>
      </a>
      <a class="m-item" href="showshop.php?resid=<?php echo $link[2]?>">
      <img src="<?php echo $images[2]?>">
      <p class="m-caption"><?php echo $names[2]?></p>
      </a>
  </div>
</div>

<div>
	<p id="hottest">TODAY'S HOTTEST!</p>
</div>

  <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

  <script src="carousel/carousel.js" type="text/javascript"></script>
  <script>$('.m-carousel').carousel({
      dragRadius: 10
    , moveRadius: 20
    , classPrefix: undefined
    , classNames: {
        outer: "carousel"
      , inner: "carousel-inner"
      , item: "item"
      , center: "center"
      , touch: "has-touch"
      , dragging: "dragging"
      , active: "active"
    }
});
</script>

</body>
</html>