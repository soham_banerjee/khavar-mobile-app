<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Browse</title>
<link rel="stylesheet" href="css/main.css">
<script>
function show(str)
{
if (str.length==0)
  { 
  document.getElementById("searchresults").innerHTML="";
  return;
  }
var xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("searchresults").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","listrestaurants.php?field="+str,true);
xmlhttp.send();
}
</script>
</head>

<body>
	
	<div id="searchbox" style="width:100%;">
<form>
<select name="views" onchange="show(this.value)" style="width:100%; height:40px;">
<option value="">Browse by:</option>
<option value="NAME">Name</option>
<option value="VOTES">Rating</option>
<option value="LOCATION">Location</option>
</select>
</form>	
	</div>
	
	<div id="searchresults" style="display:block;"></div>

</body>
</html>